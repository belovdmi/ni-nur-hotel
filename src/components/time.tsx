import { FC, useEffect, useState } from 'react';

import { Grid, Typography } from '@mui/material';
import { Brightness5 } from '@mui/icons-material';

export const formatUnit = (unit: number) =>
  unit.toString().length === 1 ? `0${unit}` : unit;

const formatTime = (date: Date) => {
  return `${formatUnit(date.getHours())}:${formatUnit(date.getMinutes())}`;
};

const Time: FC = () => {
  const [clockState, setClockState] = useState<string>(formatTime(new Date()));

  useEffect(() => {
    setInterval(() => {
      const date = new Date();
      setClockState(formatTime(date));
    }, 1000);
  }, []);

  return (
    <>
      <Typography variant="h1" fontSize={'91px'}>
        {clockState}
      </Typography>
      <Grid item container justifyContent="space-between">
        <Typography variant="h4">{new Date().toLocaleDateString()}</Typography>
        <Typography variant="h4">
          <Brightness5 sx={{ fontSize: '25px' }} /> 15 ˚C
        </Typography>
      </Grid>
    </>
  );
};

export default Time;
