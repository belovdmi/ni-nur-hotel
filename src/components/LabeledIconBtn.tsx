import { FC, PropsWithChildren } from 'react';

import { Box, IconButton, Typography } from '@mui/material';

type LabeledIconBtnProps = {
  label?: string;
  onClick?: () => void;
};

const LabeledIconBtn: FC<PropsWithChildren<LabeledIconBtnProps>> = ({
  onClick,
  label,
  children,
}) => {
  return (
    <Box textAlign={'center'} color="#1769aa">
      <IconButton
        size="large"
        sx={{ fontSize: '40px' }}
        onClick={() => {
          if (onClick) onClick();
        }}
      >
        {children}
      </IconButton>
      {label && (
        <Typography variant="subtitle1" mt="-15px">
          {label}
        </Typography>
      )}
    </Box>
  );
};

export default LabeledIconBtn;
