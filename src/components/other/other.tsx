import { FC, useState } from 'react';

import Carousel from 'react-material-ui-carousel';

import { Typography, Grid } from '@mui/material';
import { Settings, Wifi } from '@mui/icons-material';

import DashboardLabeledBtn from '../DashboardLabeledBtn';
import Modal from '../common/Modal';
import SettingsModalContent from './SettingsModalContent';
import WifiModalContent from './WifiModalContent';
import { getText } from '../../localization';

const Other: FC = () => {
  const [wifiModalOpen, setWifiModalOpen] = useState(false);
  const [settingsModalOpen, setSettingsModalOpen] = useState(false);

  return (
    <>
      <Typography variant="h4">{getText('otherSectionLabel')}</Typography>
      <Carousel
        sx={{ width: '660px', pt: 3 }}
        fullHeightHover
        navButtonsAlwaysVisible
        autoPlay={false}
        cycleNavigation={false}
        animation="slide"
      >
        <Grid
          container
          gap={3}
          wrap="nowrap"
          overflow={'hidden'}
          width="100%"
          mx={'11%'}
        >
          <DashboardLabeledBtn
            label={getText('wifiShortLabel')}
            onClick={() => setWifiModalOpen(true)}
          >
            <Wifi sx={{ fontSize: '60px' }} />
          </DashboardLabeledBtn>
          <DashboardLabeledBtn
            label={getText('settingsShortLabel')}
            onClick={() => setSettingsModalOpen(true)}
          >
            <Settings sx={{ fontSize: '60px' }} />
          </DashboardLabeledBtn>
        </Grid>
      </Carousel>

      <Modal
        header={getText('wifiLabel')}
        open={wifiModalOpen}
        onClose={() => setWifiModalOpen(false)}
      >
        <WifiModalContent />
      </Modal>

      <Modal
        header={getText('settingsLabel')}
        open={settingsModalOpen}
        onClose={() => setSettingsModalOpen(false)}
      >
        <SettingsModalContent />
      </Modal>
    </>
  );
};

export default Other;
