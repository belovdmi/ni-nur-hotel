import { FC } from 'react';

import { Grid, Typography } from '@mui/material';
import { QrCode } from '@mui/icons-material';
import { getText } from '../../localization';

const WifiModalContent: FC = () => {
  return (
    <Grid container alignItems="center" justifyContent="space-evenly">
      <Grid item>
        <Typography variant="body1">
          <b>{getText('wifiHostnameLabel')}:</b> HotelFreeWiFi
        </Typography>
        <Typography variant="body1">
          <b>{getText('wifiPasswordLabel')}:</b> 123456789
        </Typography>
      </Grid>
      <QrCode sx={{ fontSize: '100px' }} />
    </Grid>
  );
};

export default WifiModalContent;
