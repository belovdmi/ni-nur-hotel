import { FC, useContext } from 'react';

import { Grid } from '@mui/material';
import { PowerSettingsNew, Replay } from '@mui/icons-material';

import DashboardLabeledBtn from '../DashboardLabeledBtn';

import { INITIAL_SETTINGS } from '../../constants';
import SettingsContext from '../../context/SettingsContext';
import { getText } from '../../localization';

const SettingsModalContent: FC = () => {
  const [, setSettings] = useContext(SettingsContext);

  return (
    <Grid container justifyContent="space-evenly">
      <DashboardLabeledBtn
        label={getText('setDefaultsShortLabel')}
        onClick={() => setSettings({ ...INITIAL_SETTINGS })}
      >
        <Replay sx={{ fontSize: '60px' }} />
      </DashboardLabeledBtn>
      <DashboardLabeledBtn
        label={getText('turnOffShortLabel')}
        onClick={() => null}
      >
        <PowerSettingsNew sx={{ fontSize: '60px' }} />
      </DashboardLabeledBtn>
    </Grid>
  );
};

export default SettingsModalContent;
