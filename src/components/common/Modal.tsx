import { FC, ReactNode } from 'react';

import { Button, Modal as MuiModal, Typography } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { Box } from '@mui/system';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 800,
  bgcolor: '#fff',
  boxShadow: 24,
  pt: 4,
  pb: 8,
};

const exitButtonStyle = {
  position: 'absolute',
  top: '10px',
  right: '10px',
  borderRadius: '50%',
  p: 3,
};

interface IModal {
  header: string;
  open: boolean;
  onClose: () => void;
  children: ReactNode;
}

const Modal: FC<IModal> = ({ header, open, children, onClose }) => {
  return (
    <MuiModal open={open} onClose={onClose}>
      <Box sx={style}>
        <Button sx={exitButtonStyle} onClick={() => onClose()}>
          <CloseIcon />
        </Button>
        <Box
          sx={{ justifyContent: 'center', textAlign: 'center', mt: 2, mb: 8 }}
        >
          <Typography fontSize="25px" fontWeight="600">
            {header}
          </Typography>
        </Box>
        <Box sx={{ justifyContent: 'center', textAlign: 'center' }}>
          {children}
        </Box>
      </Box>
    </MuiModal>
  );
};

export default Modal;
