import { FC, useContext } from 'react';

import Carousel from 'react-material-ui-carousel';

import { Grid } from '@mui/material';
import {
  BrightnessLow,
  Forest,
  Security,
  MenuBook,
  DarkMode,
  Nightlife,
  SelfImprovement,
} from '@mui/icons-material';

import DashboardLabeledBtn from '../DashboardLabeledBtn';

import { MODES } from '../../constants';
import SettingsContext from '../../context/SettingsContext';
import { getText } from '../../localization';

const ModesModalContent: FC = () => {
  const [settings, setSettings] = useContext(SettingsContext);

  const handleChangeMode = (value: string) => {
    setSettings({ ...settings, mode: value });
  };

  return (
    <Carousel navButtonsAlwaysVisible autoPlay={false} animation="slide">
      <Grid>
        <DashboardLabeledBtn
          label={getText('modeDefaultShortLabel')}
          active={settings.mode === MODES.default ? true : false}
          onClick={() => handleChangeMode(MODES.default)}
        >
          <BrightnessLow sx={{ fontSize: '60px' }} />
        </DashboardLabeledBtn>
        <DashboardLabeledBtn
          label={getText('modeMeditationShortLabel')}
          active={settings.mode === MODES.meditation ? true : false}
          onClick={() => handleChangeMode(MODES.meditation)}
        >
          <SelfImprovement sx={{ fontSize: '60px' }} />
        </DashboardLabeledBtn>
        <DashboardLabeledBtn
          label={getText('modeEcologicalShortLabel')}
          active={settings.mode === MODES.ecological ? true : false}
          onClick={() => handleChangeMode(MODES.ecological)}
        >
          <Forest sx={{ fontSize: '60px' }} />
        </DashboardLabeledBtn>
      </Grid>
      <Grid>
        <DashboardLabeledBtn
          label={getText('modeReadingShortLabel')}
          active={settings.mode === MODES.reading ? true : false}
          onClick={() => handleChangeMode(MODES.reading)}
        >
          <MenuBook sx={{ fontSize: '60px' }} />
        </DashboardLabeledBtn>
        <DashboardLabeledBtn
          label={getText('modeDarkShortLabel')}
          active={settings.mode === MODES.dark ? true : false}
          onClick={() => handleChangeMode(MODES.dark)}
        >
          <DarkMode sx={{ fontSize: '60px' }} />
        </DashboardLabeledBtn>
        <DashboardLabeledBtn
          label={getText('modeDiscoShortLabel')}
          active={settings.mode === MODES.disco ? true : false}
          onClick={() => handleChangeMode(MODES.disco)}
        >
          <Nightlife sx={{ fontSize: '60px' }} />
        </DashboardLabeledBtn>
      </Grid>
      <Grid>
        <DashboardLabeledBtn
          label={getText('modePrivateShortLabel')}
          active={settings.mode === MODES.private ? true : false}
          onClick={() => handleChangeMode(MODES.private)}
        >
          <Security sx={{ fontSize: '60px' }} />
        </DashboardLabeledBtn>
      </Grid>
    </Carousel>
  );
};

export default ModesModalContent;
