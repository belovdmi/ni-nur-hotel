import { FC, useContext } from 'react';

import { Box, Button, Slider } from '@mui/material';
import { BlindsRounded } from '@mui/icons-material';

import { DEFAULT } from '../../constants';
import SettingsContext from '../../context/SettingsContext';
import IModalContent from '../../interfaces/IModalContent';
import { getText } from '../../localization';

const BlindsModalContent: FC<IModalContent> = ({ onClose }) => {
  const [settings, setSettings] = useContext(SettingsContext);

  return (
    <Box>
      <Box
        mx={12}
        sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignItems: 'center',
        }}
      >
        <BlindsRounded sx={{ fontSize: '75px', color: '#1769aa' }} />
        <Box>
          <Slider
            value={settings.blinds}
            onChange={(_, value) => setSettings({ ...settings, blinds: value })}
            sx={{ height: '150px' }}
            aria-label="Temperature"
            orientation="vertical"
            getAriaValueText={(value: number) => `${value} %`}
            valueLabelDisplay="auto"
            min={0}
            max={100}
            marks={[
              {
                value: 0,
                label: '0 %',
              },
              {
                value: 50,
                label: '50 %',
              },
              {
                value: 100,
                label: '100 %',
              },
            ]}
          />
        </Box>
      </Box>
      <Box mt={5} mx={14} sx={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button
          sx={{ mx: 2 }}
          onClick={() => setSettings({ ...settings, blinds: DEFAULT.blinds })}
        >
          {getText('setDefaultsLabel')}
        </Button>
        <Button sx={{ mx: 2 }} variant="contained" onClick={onClose}>
          {getText('confirmShortLabel')}
        </Button>
      </Box>
    </Box>
  );
};

export default BlindsModalContent;
