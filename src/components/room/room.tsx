import { FC, useState } from 'react';

import Carousel from 'react-material-ui-carousel';

import { Typography, Grid } from '@mui/material';
import {
  StyleRounded,
  BlindsRounded,
  ThermostatRounded,
} from '@mui/icons-material';

import DashboardLabeledBtn from '../DashboardLabeledBtn';
import Modal from '../common/Modal';
import TemperatureModalContent from './TemperatureModalContent';
import ModesModalContent from './ModesModalContent';
import BlindsModalContent from './BlindsModalContent';
import { getText } from '../../localization';

const Room: FC = () => {
  const [temperatureModalOpen, setTemperatureModalOpen] = useState(false);
  const [blindsModalOpen, setBlindsModalOpen] = useState(false);
  const [modesModalOpen, setModesModalOpen] = useState(false);

  return (
    <>
      <Typography variant="h4">{getText('roomSectionLabel')}</Typography>
      <Carousel
        sx={{ width: '660px', pt: 3 }}
        fullHeightHover
        navButtonsAlwaysVisible
        autoPlay={false}
        cycleNavigation={false}
        animation="slide"
      >
        <Grid
          container
          gap={3}
          wrap="nowrap"
          overflow={'hidden'}
          width="100%"
          mx={'11%'}
        >
          <DashboardLabeledBtn
            label={getText('temperatureShortLabel')}
            onClick={() => setTemperatureModalOpen(true)}
          >
            <ThermostatRounded sx={{ fontSize: '60px' }} />
          </DashboardLabeledBtn>
          <DashboardLabeledBtn
            label={getText('blindsShortLabel')}
            onClick={() => setBlindsModalOpen(true)}
          >
            <BlindsRounded sx={{ fontSize: '60px' }} />
          </DashboardLabeledBtn>
          <DashboardLabeledBtn
            label={getText('modesShortLabel')}
            onClick={() => setModesModalOpen(true)}
          >
            <StyleRounded sx={{ fontSize: '60px' }} />
          </DashboardLabeledBtn>
        </Grid>
      </Carousel>

      <Modal
        header={getText('temperatureLabel')}
        open={temperatureModalOpen}
        onClose={() => setTemperatureModalOpen(false)}
      >
        <TemperatureModalContent
          onClose={() => setTemperatureModalOpen(false)}
        />
      </Modal>

      <Modal
        header={getText('blindsLabel')}
        open={blindsModalOpen}
        onClose={() => setBlindsModalOpen(false)}
      >
        <BlindsModalContent onClose={() => setBlindsModalOpen(false)} />
      </Modal>

      <Modal
        header={getText('modesLabel')}
        open={modesModalOpen}
        onClose={() => setModesModalOpen(false)}
      >
        <ModesModalContent />
      </Modal>
    </>
  );
};

export default Room;
