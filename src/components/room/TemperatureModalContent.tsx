import { FC, useContext } from 'react';

import { Box, Button, Slider } from '@mui/material';
import { ThermostatRounded } from '@mui/icons-material';

import { DEFAULT } from '../../constants';
import SettingsContext from '../../context/SettingsContext';
import IModalContent from '../../interfaces/IModalContent';
import { getText } from '../../localization';

const TemperatureModalContent: FC<IModalContent> = ({ onClose }) => {
  const [settings, setSettings] = useContext(SettingsContext);

  return (
    <Box>
      <Box
        mx={12}
        sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignItems: 'center',
        }}
      >
        <ThermostatRounded sx={{ fontSize: '75px', color: '#1769aa' }} />
        <Box>
          <Slider
            value={settings.temperature}
            onChange={(_, value) =>
              setSettings({ ...settings, temperature: value })
            }
            sx={{ height: '150px' }}
            aria-label="Temperature"
            orientation="vertical"
            getAriaValueText={(value: number) => `${value}°C`}
            valueLabelDisplay="auto"
            defaultValue={[20, 37]}
            marks={[
              {
                value: 18,
                label: '18°C',
              },
              {
                value: 25,
                label: '25°C',
              },
              {
                value: 30,
                label: '30°C',
              },
              {
                value: 38,
                label: '38°C',
              },
            ]}
            max={38}
            min={18}
          />
        </Box>
      </Box>
      <Box mt={5} mx={14} sx={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button
          sx={{ mx: 2 }}
          onClick={() =>
            setSettings({ ...settings, temperature: DEFAULT.temperature })
          }
        >
          {getText('setDefaultsLabel')}
        </Button>
        <Button sx={{ mx: 2 }} variant="contained" onClick={onClose}>
          {getText('confirmShortLabel')}
        </Button>
      </Box>
    </Box>
  );
};

export default TemperatureModalContent;
