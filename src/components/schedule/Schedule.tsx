import { FC, useState } from 'react';

import { useRecoilValue, useSetRecoilState } from 'recoil';

import {
  Box,
  Button,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
} from '@mui/material';

import {
  filteredScheduleState,
  scheduleState,
} from '../../recoil/ScheduleState';
import Modal from '../common/Modal';

import { getText } from '../../localization';

const Schedule: FC = () => {
  const activities = useRecoilValue(filteredScheduleState);
  const setActivities = useSetRecoilState(scheduleState);

  const removeActivity = (name: string) => {
    const newActivities = activities.filter(
      (activity) => name !== activity.name
    );
    setActivities(newActivities);
  };

  const [confirmModalOpen, setConfirmModalOpen] = useState(false);
  const [currentActivityName, setCurrentActivityName] = useState('');
  const [currentActivityTime, setCurrentActivityTime] = useState('');

  return (
    <>
      <Table>
        <TableBody>
          {activities.map((activity, index) => (
            <TableRow
              key={activity.name}
              style={{
                background: index % 2 ? 'white' : '#eeeeee',
                height: '65px',
              }}
            >
              <TableCell>{activity.time}</TableCell>
              <TableCell>{activity.name}</TableCell>
              <TableCell>
                {activity.cancelable && (
                  <Button
                    color="error"
                    onClick={() => {
                      setConfirmModalOpen(true);
                      setCurrentActivityName(activity.name);
                      setCurrentActivityTime(activity.time);
                    }}
                  >
                    {getText('scheduleCancelLabel')}
                  </Button>
                )}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>

      <Modal
        header={getText('confirmLabel')}
        open={confirmModalOpen}
        onClose={() => setConfirmModalOpen(false)}
      >
        <Grid container alignItems="center" justifyContent="space-evenly">
          <Grid item>
            <Typography variant="body1">
              {getText('confirmAssurance')}
            </Typography>
            <Typography variant="body1">
              <b>
                {currentActivityTime} {currentActivityName}
              </b>
            </Typography>
            <Box
              mt={5}
              mx={14}
              sx={{ display: 'flex', justifyContent: 'flex-end' }}
            >
              <Button
                sx={{ ml: 50 }}
                variant="contained"
                onClick={() => {
                  removeActivity(currentActivityName);
                  setConfirmModalOpen(false);
                }}
              >
                {getText('confirmShortLabel')}
              </Button>
            </Box>
          </Grid>
        </Grid>
      </Modal>
    </>
  );
};

export default Schedule;
