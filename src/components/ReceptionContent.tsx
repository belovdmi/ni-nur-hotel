import { FC, useState, useEffect } from 'react';

import { useSetRecoilState } from 'recoil';
import { scheduleState } from '../recoil/ScheduleState';

import { Box, Typography } from '@mui/material';
import { PhoneDisabled, MicOff } from '@mui/icons-material';

import { formatUnit } from './Time';
import LabeledIconBtn from './LabeledIconBtn';
import { getText } from '../localization';

interface IReceptionContent {
  handleHangUp: () => void;
}

const ReceptionContent: FC<IReceptionContent> = ({ handleHangUp }) => {
  const [callDuration, setCallDuration] = useState(0);
  const [muted, setMuted] = useState(false);

  useEffect(() => {
    const interval = setInterval(() => {
      setCallDuration((previous) => ++previous);
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  const setActivities = useSetRecoilState(scheduleState);
  const simulatedReceptionCallEnd = () => {
    setActivities((previous) => [
      ...previous,
      {
        name: getText('scheduleMockMassageLabel'),
        time: '16:00',
        cancelable: true,
      },
    ]);
    handleHangUp();
  };

  return (
    <Box
      sx={{
        pb: 6,
      }}
    >
      <Typography>00:{formatUnit(callDuration)}</Typography>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          px: 10,
          py: 2,
        }}
      >
        <LabeledIconBtn
          label={getText('receptionMuteLabel')}
          onClick={() => setMuted((previous) => !previous)}
        >
          <MicOff
            sx={{ fontSize: '75px', color: muted ? '#aaa' : '#1769aa' }}
          />
        </LabeledIconBtn>
        <LabeledIconBtn
          label={getText('receptionEndCallLabel')}
          onClick={() => {
            simulatedReceptionCallEnd();
            setCallDuration(0);
          }}
        >
          <PhoneDisabled sx={{ fontSize: '75px', color: '#1769aa' }} />
        </LabeledIconBtn>
      </Box>
    </Box>
  );
};

export default ReceptionContent;
