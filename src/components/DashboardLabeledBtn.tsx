import { FC, PropsWithChildren } from 'react';

import { Button, Paper, Typography } from '@mui/material';

type DashboardLabeledBtnProps = {
  label: string;
  active?: boolean;
  onClick?: () => void;
};

const DashboardLabeledBtn: FC<PropsWithChildren<DashboardLabeledBtnProps>> = ({
  label,
  active,
  children,
  onClick,
}) => {
  return (
    <Button
      style={{ textTransform: 'none' }}
      onClick={() => {
        if (onClick) onClick();
      }}
    >
      <Paper
        elevation={2}
        sx={{
          p: 2.5,
          textAlign: 'center',
          minWidth: '100px',
          width: '100px',
          height: '100px',
          background: active ? '#b8dfff' : 'none',
        }}
      >
        {children}
        <Typography variant="subtitle1" pt={1}>
          {label}
        </Typography>
      </Paper>
    </Button>
  );
};

export default DashboardLabeledBtn;
