import { FC } from 'react';

import { Box, Grid, Typography } from '@mui/material';
import { getText } from '../localization';

const StayContent: FC = () => {
  return (
    <Box>
      <Grid container>
        <Grid item pl={6} pr={2} xs={6}>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between',
              textAlign: 'start',
              height: '100%',
            }}
          >
            <Box
              sx={{
                flex: '1',
                display: 'flex',
                justifyContent: 'space-between',
                textAlign: 'start',
              }}
            >
              <Typography fontSize={20}>{getText('arrivalLabel')}</Typography>
              <Typography fontSize={20}>8. 12. 2022 10:00</Typography>
            </Box>
            <Box
              sx={{
                flex: '5',
                display: 'flex',
                justifyContent: 'space-between',
                textAlign: 'start',
              }}
            >
              <Typography fontSize={20}>{getText('departureLabel')}</Typography>
              <Typography fontSize={20}>22. 12. 2022 10:00</Typography>
            </Box>
            <Box
              sx={{
                flex: '6',
                display: 'flex',
                justifyContent: 'space-between',
                textAlign: 'start',
                alignContent: 'center',
              }}
            >
              <Typography fontSize={36}>{getText('debtLabel')}</Typography>
              <Typography fontSize={36}>50$</Typography>
            </Box>
          </Box>
        </Grid>
        <Grid
          container
          item
          xs={6}
          pr={6}
          pl={2}
          sx={{ height: '400px', overflowY: 'auto', textAlign: 'start' }}
        >
          <ul>
            {[...Array(25)].map((_, i) => (
              <li key={i}>
                <Typography>{getText('debtItemLabel')} 2$</Typography>
              </li>
            ))}
          </ul>
        </Grid>
      </Grid>
    </Box>
  );
};

export default StayContent;
