import { FC, useContext } from 'react';

import { HexColorPicker } from 'react-colorful';

import { Box, Button, Slider, Typography } from '@mui/material';
import { PowerSettingsNew } from '@mui/icons-material';

import LabeledIconBtn from '../LabeledIconBtn';

import { DEFAULT } from '../../constants';
import SettingsContext from '../../context/SettingsContext';
import IModalContent from '../../interfaces/IModalContent';
import { getText } from '../../localization';

const HiddenLightModalContent: FC<IModalContent> = ({ onClose }) => {
  const [settings, setSettings] = useContext(SettingsContext);

  return (
    <Box>
      <Box
        mx={12}
        sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignItems: 'center',
        }}
      >
        <Box
          sx={{
            flex: 1,
          }}
        >
          <LabeledIconBtn
            onClick={() =>
              setSettings({
                ...settings,
                lightHiddenOn: !settings.lightHiddenOn,
              })
            }
          >
            <PowerSettingsNew
              sx={{
                fontSize: '75px',
                color: settings.lightHiddenOn ? '#1769aa' : '#aaa',
              }}
            />
          </LabeledIconBtn>
        </Box>
        <Box
          sx={{
            flex: 2,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            alignItems: 'center',
          }}
        >
          <Box>
            <Typography pb={2}>
              {getText('lightIntensityShortLabel')}
            </Typography>
            <Slider
              value={settings.lightHiddenIntensity}
              onChange={(_, value) =>
                setSettings({ ...settings, lightHiddenIntensity: value })
              }
              sx={{ height: '150px' }}
              aria-label="Intensity"
              orientation="vertical"
              getAriaValueText={(value: number) => `${value}%`}
              valueLabelDisplay="auto"
              defaultValue={80}
              disabled={!settings.lightHiddenOn}
            />
          </Box>
          <HexColorPicker
            color={settings.lightHiddenColor}
            onChange={(value: string) =>
              setSettings({ ...settings, lightHiddenColor: value })
            }
          />
        </Box>
      </Box>
      <Box mt={5} mx={14} sx={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button
          sx={{ mx: 2 }}
          onClick={() =>
            setSettings({
              ...settings,
              lightHiddenOn: DEFAULT.lightOn,
              lightHiddenIntensity: DEFAULT.lightIntensity,
              lightHiddenColor: DEFAULT.lightColor,
            })
          }
        >
          {getText('setDefaultsLabel')}
        </Button>
        <Button sx={{ mx: 2 }} variant="contained" onClick={onClose}>
          {getText('confirmShortLabel')}
        </Button>
      </Box>
    </Box>
  );
};

export default HiddenLightModalContent;
