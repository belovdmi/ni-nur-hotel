import { FC, useContext, useState } from 'react';

import Carousel from 'react-material-ui-carousel';

import { Typography, Grid } from '@mui/material';
import { Lightbulb } from '@mui/icons-material';

import Modal from '../common/Modal';
import DashboardLabeledBtn from '../DashboardLabeledBtn';

import MainLightModalContent from './MainLightModalContent';
import HallLightModalContent from './HallLightModalContent';
import BedLightModalContent from './BedLightModalContent';
import BathroomLightModalContent from './BathroomLightModalContent';
import ToiletLightModalContent from './ToiletLightModalContent';
import PantryLightModalContent from './PantryLightModalContent';
import HiddenLightModalContent from './HiddenLightModalContent';

import SettingsContext from '../../context/SettingsContext';
import { getText } from '../../localization';

const Lights: FC = () => {
  const [settings] = useContext(SettingsContext);

  const [lightMainModalOpen, setLightMainModalOpen] = useState(false);
  const [lightHallModalOpen, setLightHallModalOpen] = useState(false);
  const [lightBedModalOpen, setLightBedModalOpen] = useState(false);
  const [lightBathroomModalOpen, setLightBathroomModalOpen] = useState(false);
  const [lightToiletModalOpen, setLightToiletModalOpen] = useState(false);
  const [lightPantryModalOpen, setLightPantryModalOpen] = useState(false);
  const [lightHiddenModalOpen, setLightHiddenModalOpen] = useState(false);

  return (
    <>
      <Typography variant="h4">{getText('lightsSectionLabel')}</Typography>

      <Carousel
        sx={{ minWidth: '660px', pt: 3, width: '100%' }}
        fullHeightHover
        navButtonsAlwaysVisible
        autoPlay={false}
        animation="slide"
      >
        <Grid
          container
          gap={3}
          wrap="nowrap"
          overflow={'hidden'}
          width="100%"
          mx={'11%'}
        >
          <DashboardLabeledBtn
            onClick={() => setLightMainModalOpen(true)}
            label={getText('lightMainShortLabel')}
          >
            <Lightbulb
              sx={{
                color: settings.lightMainOn ? settings.lightMainColor : 'black',
                fontSize: '60px',
              }}
            />
          </DashboardLabeledBtn>
          <DashboardLabeledBtn
            onClick={() => setLightHallModalOpen(true)}
            label={getText('lightHallShortLabel')}
          >
            <Lightbulb
              sx={{
                color: settings.lightHallOn ? settings.lightHallColor : 'black',
                fontSize: '60px',
              }}
            />
          </DashboardLabeledBtn>
          <DashboardLabeledBtn
            onClick={() => setLightBedModalOpen(true)}
            label={getText('lightBedShortLabel')}
          >
            <Lightbulb
              sx={{
                color: settings.lightBedOn ? settings.lightBedColor : 'black',
                fontSize: '60px',
              }}
            />
          </DashboardLabeledBtn>
        </Grid>
        <Grid
          container
          gap={3}
          wrap="nowrap"
          overflow={'hidden'}
          width="100%"
          mx={'11%'}
        >
          <DashboardLabeledBtn
            onClick={() => setLightBathroomModalOpen(true)}
            label={getText('lightBathroomShortLabel')}
          >
            <Lightbulb
              sx={{
                color: settings.lightBathroomOn
                  ? settings.lightBathroomColor
                  : 'black',
                fontSize: '60px',
              }}
            />
          </DashboardLabeledBtn>
          <DashboardLabeledBtn
            onClick={() => setLightToiletModalOpen(true)}
            label={getText('lightToiletShortLabel')}
          >
            <Lightbulb
              sx={{
                color: settings.lightToiletOn
                  ? settings.lightToiletColor
                  : 'black',
                fontSize: '60px',
              }}
            />
          </DashboardLabeledBtn>
          <DashboardLabeledBtn
            onClick={() => setLightPantryModalOpen(true)}
            label={getText('lightPantryShortLabel')}
          >
            <Lightbulb
              sx={{
                color: settings.lightPantryOn
                  ? settings.lightPantryColor
                  : 'black',
                fontSize: '60px',
              }}
            />
          </DashboardLabeledBtn>
        </Grid>
        <Grid
          container
          gap={3}
          wrap="nowrap"
          overflow={'hidden'}
          width="100%"
          mx={'11%'}
        >
          <DashboardLabeledBtn
            onClick={() => setLightHiddenModalOpen(true)}
            label={getText('lightHiddenShortLabel')}
          >
            <Lightbulb
              sx={{
                color: settings.lightHiddenOn
                  ? settings.lightHiddenColor
                  : 'black',
                fontSize: '60px',
              }}
            />
          </DashboardLabeledBtn>
        </Grid>
      </Carousel>

      <Modal
        header={getText('lightMainLabel')}
        open={lightMainModalOpen}
        onClose={() => setLightMainModalOpen(false)}
      >
        <MainLightModalContent onClose={() => setLightMainModalOpen(false)} />
      </Modal>

      <Modal
        header={getText('lightHallLabel')}
        open={lightHallModalOpen}
        onClose={() => setLightHallModalOpen(false)}
      >
        <HallLightModalContent onClose={() => setLightHallModalOpen(false)} />
      </Modal>

      <Modal
        header={getText('lightBedLabel')}
        open={lightBedModalOpen}
        onClose={() => setLightBedModalOpen(false)}
      >
        <BedLightModalContent onClose={() => setLightBedModalOpen(false)} />
      </Modal>

      <Modal
        header={getText('lightBathroomLabel')}
        open={lightBathroomModalOpen}
        onClose={() => setLightBathroomModalOpen(false)}
      >
        <BathroomLightModalContent
          onClose={() => setLightBathroomModalOpen(false)}
        />
      </Modal>

      <Modal
        header={getText('lightToiletLabel')}
        open={lightToiletModalOpen}
        onClose={() => setLightToiletModalOpen(false)}
      >
        <ToiletLightModalContent
          onClose={() => setLightToiletModalOpen(false)}
        />
      </Modal>

      <Modal
        header={getText('lightPantryLabel')}
        open={lightPantryModalOpen}
        onClose={() => setLightPantryModalOpen(false)}
      >
        <PantryLightModalContent
          onClose={() => setLightPantryModalOpen(false)}
        />
      </Modal>

      <Modal
        header={getText('lightHiddenLabel')}
        open={lightHiddenModalOpen}
        onClose={() => setLightHiddenModalOpen(false)}
      >
        <HiddenLightModalContent
          onClose={() => setLightHiddenModalOpen(false)}
        />
      </Modal>
    </>
  );
};

export default Lights;
