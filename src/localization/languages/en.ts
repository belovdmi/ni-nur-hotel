import { ILanguage } from '../../interfaces';

const en: ILanguage = {
  stayLabel: 'Information about my stay',
  stayShortLabel: 'My stay',

  arrivalLabel: 'Arrival',
  departureLabel: 'Departure',
  debtLabel: 'Spent',
  debtItemLabel: 'The item of money spent',

  receptionLabel: 'Calling reception',
  receptionShortLabel: 'Reception',
  receptionMuteLabel: 'Mute',
  receptionEndCallLabel: 'End call',

  scheduleMockBreakfastLabel: 'Breakfast',
  scheduleMockLunchLabel: 'Lunch',
  scheduleMockDinnerLabel: 'Dinner',

  scheduleMockMeetingLabel: 'Meeting',
  scheduleMockMassageLabel: 'Massage',
  scheduleMockCinemaLabel: 'Cinema',

  scheduleCancelLabel: 'Cancel',

  doNotInteruptLabel: 'Do not interupt',
  cleanMyRoomLabel: 'Clean my room',

  lightsSectionLabel: 'Lights',
  roomSectionLabel: 'Room',
  otherSectionLabel: 'Other',

  lightMainLabel: 'Main light',
  lightMainShortLabel: 'Main',

  lightHallLabel: 'Hall light',
  lightHallShortLabel: 'Hall',

  lightBedLabel: 'Bed light',
  lightBedShortLabel: 'Bed',

  lightBathroomLabel: 'Bathroom light',
  lightBathroomShortLabel: 'Bathroom',

  lightToiletLabel: 'Toilet light',
  lightToiletShortLabel: 'Toilet',

  lightPantryLabel: 'Pantry light',
  lightPantryShortLabel: 'Pantry',

  lightHiddenLabel: 'Hidden light',
  lightHiddenShortLabel: 'Hidden',

  lightIntensityShortLabel: 'Intensity',

  temperatureLabel: 'Temperature',
  temperatureShortLabel: 'Temperature',

  blindsLabel: 'Blinds',
  blindsShortLabel: 'Blinds',

  modesLabel: 'Modes',
  modesShortLabel: 'Modes',

  modeDefaultShortLabel: 'Default',
  modeMeditationShortLabel: 'Meditation',
  modeEcologicalShortLabel: 'Eco',
  modeReadingShortLabel: 'Reading',
  modeDarkShortLabel: 'Dark',
  modeDiscoShortLabel: 'Disco',
  modePrivateShortLabel: 'Private',

  wifiLabel: 'Wi-Fi',
  wifiShortLabel: 'Wi-Fi',
  wifiHostnameLabel: 'Hostname',
  wifiPasswordLabel: 'Password',

  settingsLabel: 'Settings',
  settingsShortLabel: 'Settings',

  setDefaultsLabel: 'Set defaults',
  setDefaultsShortLabel: 'Reset',

  confirmLabel: 'Confirmation',
  confirmShortLabel: 'OK',
  confirmAssurance: 'Are you sure you want to cancel this event?',

  turnOffShortLabel: 'Turn off',
};

export default en;
