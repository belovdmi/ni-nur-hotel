import { ILanguage } from '../../interfaces';

const cz: ILanguage = {
  stayLabel: 'Informace o mém pobytu',
  stayShortLabel: 'Můj pobyt',

  arrivalLabel: 'Příjezd',
  departureLabel: 'Odjezd',
  debtLabel: 'Útrata',
  debtItemLabel: 'Položka útraty',

  receptionLabel: 'Volání na recepci',
  receptionShortLabel: 'Recepce',
  receptionMuteLabel: 'Ztlumit',
  receptionEndCallLabel: 'Ukončit hovor',

  scheduleMockBreakfastLabel: 'Snídaně',
  scheduleMockLunchLabel: 'Oběd',
  scheduleMockDinnerLabel: 'Večeře',

  scheduleMockMeetingLabel: 'Schůzka',
  scheduleMockMassageLabel: 'Masáž',
  scheduleMockCinemaLabel: 'Kino',

  scheduleCancelLabel: 'Zrušit',

  doNotInteruptLabel: 'Nerušit',
  cleanMyRoomLabel: 'Uklidit',

  lightsSectionLabel: 'Světla',
  roomSectionLabel: 'Místnost',
  otherSectionLabel: 'Ostatní',

  lightMainLabel: 'Hlavní světlo',
  lightMainShortLabel: 'Hlavní',

  lightHallLabel: 'Světlo v předsíni',
  lightHallShortLabel: 'Předsíň',

  lightBedLabel: 'Světlo v ložnici',
  lightBedShortLabel: 'Ložnice',

  lightBathroomLabel: 'Světlo v koupelně',
  lightBathroomShortLabel: 'Koupelna',

  lightToiletLabel: 'Světlo na záchodě',
  lightToiletShortLabel: 'Záchod',

  lightPantryLabel: 'Světlo ve spíži',
  lightPantryShortLabel: 'Spíž',

  lightHiddenLabel: 'Skryté světlo',
  lightHiddenShortLabel: 'Skryté',

  lightIntensityShortLabel: 'Intenzita',

  temperatureLabel: 'Teplota',
  temperatureShortLabel: 'Teplota',

  blindsLabel: 'Rolety',
  blindsShortLabel: 'Rolety',

  modesLabel: 'Módy',
  modesShortLabel: 'Módy',

  modeDefaultShortLabel: 'Výchozí',
  modeMeditationShortLabel: 'Meditace',
  modeEcologicalShortLabel: 'Eko',
  modeReadingShortLabel: 'Čtení',
  modeDarkShortLabel: 'Temno',
  modeDiscoShortLabel: 'Disko',
  modePrivateShortLabel: 'Soukromý',

  wifiLabel: 'Údaje o Wi-Fi',
  wifiShortLabel: 'Wi-Fi',
  wifiHostnameLabel: 'Název sítě',
  wifiPasswordLabel: 'Heslo',

  settingsLabel: 'Nastavení',
  settingsShortLabel: 'Nastavení',

  setDefaultsLabel: 'Nastavit výchozí hodnoty',
  setDefaultsShortLabel: 'Reset',

  confirmLabel: 'Potvrzení',
  confirmShortLabel: 'OK',
  confirmAssurance: 'Opravdu chcete zrušit tuto událost?',

  turnOffShortLabel: 'Vypnout',
};

export default cz;
