import { ILanguage } from '../interfaces';
import { cz } from './languages';

export const getText = (key: keyof ILanguage) => cz[key];
