export default interface IDialogContent {
  onClose: () => void;
}
