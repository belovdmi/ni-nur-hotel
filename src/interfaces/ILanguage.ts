export default interface ILanguage {
  stayLabel: string;
  stayShortLabel: string;

  arrivalLabel: string;
  departureLabel: string;
  debtLabel: string;
  debtItemLabel: string;

  receptionLabel: string;
  receptionShortLabel: string;
  receptionMuteLabel: string;
  receptionEndCallLabel: string;

  scheduleMockBreakfastLabel: string;
  scheduleMockLunchLabel: string;
  scheduleMockDinnerLabel: string;

  scheduleMockMeetingLabel: string;
  scheduleMockMassageLabel: string;
  scheduleMockCinemaLabel: string;

  scheduleCancelLabel: string;

  doNotInteruptLabel: string;
  cleanMyRoomLabel: string;

  lightsSectionLabel: string;
  roomSectionLabel: string;
  otherSectionLabel: string;

  lightMainLabel: string;
  lightMainShortLabel: string;

  lightHallLabel: string;
  lightHallShortLabel: string;

  lightBedLabel: string;
  lightBedShortLabel: string;

  lightBathroomLabel: string;
  lightBathroomShortLabel: string;

  lightToiletLabel: string;
  lightToiletShortLabel: string;

  lightPantryLabel: string;
  lightPantryShortLabel: string;

  lightHiddenLabel: string;
  lightHiddenShortLabel: string;

  lightIntensityShortLabel: string;

  temperatureLabel: string;
  temperatureShortLabel: string;

  blindsLabel: string;
  blindsShortLabel: string;

  modesLabel: string;
  modesShortLabel: string;

  modeDefaultShortLabel: string;
  modeMeditationShortLabel: string;
  modeEcologicalShortLabel: string;
  modeReadingShortLabel: string;
  modeDarkShortLabel: string;
  modeDiscoShortLabel: string;
  modePrivateShortLabel: string;

  wifiLabel: string;
  wifiShortLabel: string;
  wifiHostnameLabel: string;
  wifiPasswordLabel: string;

  settingsLabel: string;
  settingsShortLabel: string;

  setDefaultsLabel: string;
  setDefaultsShortLabel: string;

  confirmLabel: string;
  confirmShortLabel: string;
  confirmAssurance: string;

  turnOffShortLabel: string;
}
