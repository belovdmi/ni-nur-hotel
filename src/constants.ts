export const MODES = {
  default: 'default',
  meditation: 'meditation',
  ecological: 'ecological',
  reading: 'reading',
  dark: 'dark',
  disco: 'disco',
  private: 'private',
};

export const DEFAULT = {
  lightOn: false,
  lightIntensity: 80,
  lightColor: '#FFE000',
  temperature: 25,
  blinds: 50,
  mode: MODES.default,
};

export const INITIAL_SETTINGS = {
  doNotInterupt: false,
  cleanMyRoom: false,

  lightMainOn: DEFAULT.lightOn,
  lightMainIntensity: DEFAULT.lightIntensity,
  lightMainColor: DEFAULT.lightColor,

  lightHallOn: DEFAULT.lightOn,
  lightHallIntensity: DEFAULT.lightIntensity,
  lightHallColor: DEFAULT.lightColor,

  lightBedOn: DEFAULT.lightOn,
  lightBedIntensity: DEFAULT.lightIntensity,
  lightBedColor: DEFAULT.lightColor,

  lightBathroomOn: DEFAULT.lightOn,
  lightBathroomIntensity: DEFAULT.lightIntensity,
  lightBathroomColor: DEFAULT.lightColor,

  lightToiletOn: DEFAULT.lightOn,
  lightToiletIntensity: DEFAULT.lightIntensity,
  lightToiletColor: DEFAULT.lightColor,

  lightPantryOn: DEFAULT.lightOn,
  lightPantryIntensity: DEFAULT.lightIntensity,
  lightPantryColor: DEFAULT.lightColor,

  lightHiddenOn: DEFAULT.lightOn,
  lightHiddenIntensity: DEFAULT.lightIntensity,
  lightHiddenColor: DEFAULT.lightColor,

  temperature: DEFAULT.temperature,
  blinds: DEFAULT.blinds,
  mode: DEFAULT.mode,
};
