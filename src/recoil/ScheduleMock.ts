import { IActivity } from './ScheduleState';
import { getText } from '../localization';

export const ScheduleMock: IActivity[] = [
  {
    time: '8:00',
    name: getText('scheduleMockBreakfastLabel'),
    cancelable: false,
  },
  { time: '12:00', name: getText('scheduleMockLunchLabel'), cancelable: false },
  {
    time: '15:00',
    name: getText('scheduleMockMeetingLabel'),
    cancelable: true,
  },
  {
    time: '18:00',
    name: getText('scheduleMockDinnerLabel'),
    cancelable: false,
  },
  { time: '21:00', name: getText('scheduleMockCinemaLabel'), cancelable: true },
];
