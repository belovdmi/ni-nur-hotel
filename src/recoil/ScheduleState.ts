import { atom, selector } from 'recoil';

import { ScheduleMock } from './ScheduleMock';

export interface IActivity {
  time: string;
  name: string;
  cancelable: boolean;
}

export const scheduleState = atom<IActivity[]>({
  key: 'activities',
  default: ScheduleMock,
});

export const filteredScheduleState = selector({
  key: 'filteredActivities',
  get: ({ get }) => {
    const activities = get(scheduleState);
    return [...activities].sort(
      (a, b) => parseInt(a.time.split(':')[0]) - parseInt(b.time.split(':')[0])
    );
  },
});
