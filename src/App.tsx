import { useState } from 'react';

import { Box, Button, Collapse, Grid, IconButton } from '@mui/material';
import {
  ArrowBackIosNewRounded,
  ArrowForwardIosRounded,
  CallRounded,
  PersonRounded,
} from '@mui/icons-material';

import Lights from './components/lights/Lights';
import Room from './components/room/Room';
import Other from './components/other/Other';
import Time from './components/Time';
import LabeledIconBtn from './components/LabeledIconBtn';
import Schedule from './components/schedule/Schedule';
import Modal from './components/common/Modal';
import StayContent from './components/StayContent';
import ReceptionContent from './components/ReceptionContent';

import { INITIAL_SETTINGS } from './constants';
import SettingsContext from './context/SettingsContext';
import { getText } from './localization';

function App() {
  const [settings, setSettings] = useState({ ...INITIAL_SETTINGS });

  const [sideOpen, setSideOpen] = useState(true);

  const [callModalOpen, setCallModalOpen] = useState(false);
  const [stayModalOpen, setStayModalOpen] = useState(false);

  return (
    <SettingsContext.Provider value={[settings, setSettings]}>
      <Grid
        container
        sx={{
          position: 'relative',
          width: '100%',
          height: '100vh',
          overflowX: 'hidden',
        }}
      >
        <Collapse
          orientation="horizontal"
          collapsedSize={(1 / 3) * window.innerWidth}
          in={sideOpen}
        >
          <Grid
            sx={{
              width: String((2 / 3) * window.innerWidth) + 'px',
              height: '100%',
            }}
            p={4}
            container
            direction="column"
            justifyContent="space-between"
          >
            <Grid container item>
              <Grid item xs={5}>
                <Time />
              </Grid>
              <Grid
                xs={7}
                item
                container
                justifyContent={'flex-end'}
                gap={3}
                pr={2}
              >
                <LabeledIconBtn
                  label={getText('stayShortLabel')}
                  onClick={() => setStayModalOpen(true)}
                >
                  <PersonRounded sx={{ fontSize: '75px', color: '#1769aa' }} />
                </LabeledIconBtn>
                <LabeledIconBtn
                  label={getText('receptionShortLabel')}
                  onClick={() => setCallModalOpen(true)}
                >
                  <CallRounded sx={{ fontSize: '75px', color: '#1769aa' }} />
                </LabeledIconBtn>
              </Grid>
            </Grid>

            <Box sx={{ maxHeight: '50%', overflow: 'auto' }}>
              <Schedule />
            </Box>

            <Grid container spacing={3}>
              <Grid item xs={6}>
                <Button
                  variant={settings.doNotInterupt ? 'contained' : 'outlined'}
                  fullWidth
                  size="small"
                  onClick={() =>
                    setSettings({
                      ...settings,
                      doNotInterupt: !settings.doNotInterupt,
                      cleanMyRoom: false,
                    })
                  }
                >
                  {getText('doNotInteruptLabel')}
                </Button>
              </Grid>
              <Grid item xs={6}>
                <Button
                  variant={settings.cleanMyRoom ? 'contained' : 'outlined'}
                  fullWidth
                  size="small"
                  onClick={() =>
                    setSettings({
                      ...settings,
                      doNotInterupt: false,
                      cleanMyRoom: !settings.cleanMyRoom,
                    })
                  }
                >
                  {getText('cleanMyRoomLabel')}
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Collapse>

        {/* Layout button */}
        <Grid item xs={1} textAlign="center" width={'100%'}>
          <Box position={'relative'} top="46%">
            <IconButton
              style={{ backgroundColor: '#fff' }}
              onClick={() => {
                setSideOpen(!sideOpen);
              }}
            >
              {sideOpen ? (
                <ArrowBackIosNewRounded
                  sx={{ fontSize: '45px', color: '#1769aa' }}
                />
              ) : (
                <ArrowForwardIosRounded
                  sx={{ fontSize: '45px', color: '#1769aa' }}
                />
              )}
            </IconButton>
          </Box>
          <Box height={'90vh'} bgcolor="#1769aa" width={'2px'} ml={6} />
        </Grid>

        {/* Control center */}
        <Grid
          container
          item
          justifyContent="space-between"
          p={4}
          pl={0}
          width="100px"
          height="100vh"
        >
          <Grid xs={12} item>
            <Lights />
          </Grid>
          <Grid xs={12} item>
            <Room />
          </Grid>
          <Grid xs={12} item>
            <Other />
          </Grid>
        </Grid>

        <Modal
          header={getText('receptionLabel')}
          open={callModalOpen}
          onClose={() => setCallModalOpen(false)}
        >
          <ReceptionContent handleHangUp={() => setCallModalOpen(false)} />
        </Modal>

        <Modal
          header={getText('stayLabel')}
          open={stayModalOpen}
          onClose={() => setStayModalOpen(false)}
        >
          <StayContent />
        </Modal>
      </Grid>
    </SettingsContext.Provider>
  );
}

export default App;
